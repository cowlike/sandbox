package sandbox.java;

public class Person {
	private String firstName;
	private String lastName;
	private Status status = Status.OFFLINE;
	
	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getName() {
		return lastName + ", " + firstName;
	}
	
	public void setStatus(Status newStatus) {
		status = newStatus;
	}
	
	public String toString() {
		return firstName + " " + lastName + " is " + status;
	}
}
