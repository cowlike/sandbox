(ns sandbox.0828)

(let [[a c] [inc 2]] (a c))

(let [[[a c] b] [[+ 1] 2]] (a c b))

(let [[a c] [+ (range 3)]] (apply a c))

;;#147
(defn ptrap [xs]
  (iterate 
    #(flatten (vector 
                (first %)
                (map (partial apply +) (partition 2 1 %))
                (last %))) 
    xs))

(= (second (ptrap [2 3 2])) [2 5 5 2])

