(ns sandbox.0806)

(defn iseq [s]
  (loop [s s n 0 acc []]
    (if (empty? s)
      acc
      (recur (rest s) (inc n) (conj acc [(first s) n])))))

(defn iseq [s]
  (into '() (zipmap s (range))))

(defn iseq [s]
  (map vector s (range)))
  
(= (iseq [:a :b :c]) [[:a 0] [:b 1] [:c 2]])

;[1]
;[1 1]
;[1 2 1]
;[1 3 3 1]
;[1 4 6 4 1]

(defn pascal
  "Builds Pascal's triangle up starting from row 1 to the desired row"
  [row]
  (condp = row
    1 [1]
    2 [1 1]
    (loop [acc [1 1] currow 2]
      (if (= currow row)
        acc
        (let [pairs (partition 2 1 acc)]
          (recur 
            (flatten (vector 1 (map (partial apply +) pairs) 1))
            (inc currow)))))))

(defn pascal2
  "Builds Pascal's triangle up starting from row 1 to the desired row"
  [row]
  (if (= row 1) 
    [1]
    (loop [acc [1] currow 1]
      (if (= currow row)
        acc
        (let [pairs (partition 2 1 acc)]
          (recur (concat [1] (map (partial apply +) pairs) [1])
                 (inc currow)))))))

(defn pascal-iter
  "Builds infinite Pascal's triangle up starting from row 1"
  []
  (letfn [(mkrow [r] (concat [1N] 
                             (map (partial apply +) (partition 2 1 r)) 
                             [1N]))]
    (iterate mkrow [1N])))

(defn pascal-back
  "Given the desired row of Pascal's triangle, calculate the values by
  summing all the adjacent values above each position (works backward to row 1)"
  [row]
  (if (= row 1)
    [1]
    (for [x (range row)]
      (let [prev-row (pascal-back (dec row))]
        (+ (nth prev-row (dec x) 0) (nth prev-row x 0))))))

(= (pascal 5) (pascal-back 5) (pascal2 5))

(defn mymap [f [x & xs]]
  (cons (f x) 
        (lazy-seq (when xs (mymap f xs)))))

(defn mymap2
  "The cons of the initial value can be part of the lazy-seq body expressions"
  [f [x & xs]]
  (lazy-seq (cons (f x) (when xs (mymap f xs)))))

(= [3 4 5 6 7]
   (mymap inc [2 3 4 5 6])
   (mymap2 inc [2 3 4 5 6]))

(defn third [[_ _ x]]
  x)

(defn nrest [[_ _ & more]]
  more)

