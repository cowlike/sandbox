(ns sandbox.1001)


;problem #43
(defn inter [xs n]
  (let [len (quot (count xs) n)]
    (partition len (apply interleave (partition n xs)))))

(= (inter (range 9) 3) '((0 3 6) (1 4 7) (2 5 8)))

(= (inter [1 2 3 4 5 6] 2) '((1 3 5) (2 4 6)))

;problem #50
(defn tsplit [xs]
  (into #{} (vals (group-by type xs))))

(def tsplit (comp vals (partial group-by type)))

(= (set (tsplit [[1 2] :a [3 4] 5 6 :b])) #{[[1 2] [3 4]] [:a :b] [5 6]})

;problem #55
(defn distincts [xs]
  (into {} 
        (map #(vector (key %) (count (val %)))
             (group-by identity xs))))

(defn distincts [xs]
  (->> xs
    (group-by identity)
    (mapcat #(vector (first %) (count (second %))))
    (apply hash-map)))

;;This version will only work in Clojure 1.7 when they added (update)
;;but 4clojure.com only support Clojure 1.6
(defn distincts [xs]
  (loop [m (group-by identity xs) 
         [k & ks] (keys m)]
    (if (nil? k) m (recur (update m k count) ks))))
;;or
(defn distincts [xs]
  (let [m (group-by identity xs)]
    (reduce #(update %1 %2 count) m (keys m)))) 

(= (distincts [1 1 2 3 2 1 1]) {1 4, 2 2, 3 1})
(= (distincts [:b :a :b :a :b]) {:a 2, :b 3})
(= (distincts '([1 2] [1 3] [1 3])) {[1 2] 1, [1 3] 2})
