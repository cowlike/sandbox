(ns sandbox.0508)

(defn pal? [xs]
  (->> xs
    reverse
    (= (seq xs))))

(defn pal2? [xs]
  (let [ys (seq xs)]
    (pal? ys)))

(defn pal3? [xs]
  (= (seq xs) 
     (into '() xs)))

;; using anonymous shorthand
#(= (seq %)
    (into '() %))

#(= (seq %)
    (reverse %))

(true? (pal? [:foo :bar :foo]))
(true? (pal? '(1 1 3 3 1 1)))
(true? (pal? "racecar"))

(true? = (#(into '() (reverse %1) "racecar")))


(reverse "racecar")

(into '() "racecar")

(defn pal? [ys]
  (loop [xs ys acc '() acc2 []]
    (if (empty? xs)
      (= acc2 acc)
      (recur (rest xs) (cons (first xs) acc) (conj acc2 (first xs))))))

;;========================================
(defn fib [x]
  (take x [1 1 2 3]))

(defn fib [y]
  (loop [x y fibs [1 1]]
    (cond (<= y 0) nil
          (= y 1) [1]
          (= y 2) [1 1]
          :otherwise (if (zero? (- x 2))
                       fibs
                       (recur (dec x) (conj fibs 
                                            (+ (last fibs)
                                               (last (butlast fibs)))))))))

(defn fib [n]
  (->> [1 1] 
    (iterate #(cons (+ (first %) (second %)) %))
    (take (dec n))
    last
    reverse))

(defn fib [n]
  (letfn [(fibs [a b] (cons a (lazy-seq (fibs b  (+ a b)))))]
    (take n (fibs 1 1))))

(defn fib [n]
  (take n (#(cons %1 (lazy-seq (fibs %2  (+ %1 %2)))) 1 1)))