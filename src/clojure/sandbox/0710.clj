(ns sandbox.0710)

(defn gcd [a b]
  (if 
    (zero? b) a
    (recur b (mod a b))))

(= (gcd 1023 858) 33)

(defn rel [less x y]
  (cond
    (less x y) :lt
    (less y x) :gt
    :else :eq))
    

(defn intersect [s1 s2]
  (reduce (fn [t v] 
            (if (contains? s1 v)
              (conj t v)
              t))
          #{} s2))

(= (intersect #{:a :b :c :d} #{:c :e :a :f :d}) #{:a :c :d})

(defn iter [f v]
  (cons v (lazy-seq (iter f (f v)))))

(= (take 100 (iter inc 0)) (take 100 (range)))