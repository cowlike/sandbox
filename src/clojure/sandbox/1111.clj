(ns sandbox.1111)

(def mydiv 
  (partial + 2))

(= (mydiv 2 3) 7)


(defn happynum [n]
  (loop [x n sqacc #{}]
    (cond 
      (= x 1) true
      (contains? sqacc x) false
      :else (let [ss (reduce + (loop [acc '() x x]
                               (if (= x 0) acc
                                 (recur (let [y (mod x 10)] (conj acc (* y y))) (quot x 10)))))] (recur ss (conj sqacc x))))))

(= (happynum 986543210) true)