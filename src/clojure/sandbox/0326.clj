(ns sandbox.0326)

(def data "test")

(defn foo []
  (prn data)
  (let [data [1 2 3]]
    (prn data)
    (reduce + data)))

(defn foo2 [lst]
  (* 2 
     (reduce + 
             (map inc lst))))

(defn foo3 [lst s]
  (->> lst
    (map inc)
    (reduce +)
    (* 2)))
  
(->> 2 ((fn [arg] (str "x" "y" arg "z"))))

(->> 2 (#(str "x" "y" % "z")))

(-> 2 (str "x" "y" "z"))

(->> 2 (str "x" "y" "z"))

(apply map [inc [1 2]])
(reduce map [inc [1 2]])
(map inc [1 2])

(apply map [+ [1 2] [5 8]])


(:a {:b 1 :a 3 :c :x})
({:b 1 :a 3 :c :x} :a)
(:x {:b 1 :a 3 :c :x})

(vals {:b 1 :a 3 :c :x})

#{3 :b :a} ;;set
{:a 1 :b 2} ;;map

(inc 10) ;;a list where the first argument is a function to eval
'(inc 10) ;; a literal list (won't evaluate the function)

(apply + '(1 2 3))

;;4clojure 134
(defn nil-key [k m]
  (let [result (find m k)]
    (if (nil? result)
      false
      (nil? (second result)))))

(for [x (range 2 11) :when (even? x)] x)