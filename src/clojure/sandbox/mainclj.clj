(ns sandbox.mainclj
  (:import [sandbox.java Person Status])
  (:gen-class))

(defn -main [& args]
  (println "Hello world from Clojure.")
  (let [p (Person. "John" "Smith")]
    (.setStatus p Status/OFFLINE)
    (println (str p))
    (.setStatus p Status/ONLINE)
    (println (str p)))) 
    
