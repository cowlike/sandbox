(ns sandbox.4clojure)

(defn fib 
  ([] (fib 1 1))
  ([x y] (lazy-seq (cons x (fib y (+ x y))))))

;;Haskell
;;let primes = sieve [2..] where sieve (x:xs) = x:(sieve [y | y <- xs, mod y x > 0])

(defn primes []
  (letfn [(sieve [[x & xs]]
            (lazy-seq (cons x (sieve (for [y xs :when (pos? (mod y x))] y)))))]
    (sieve (iterate inc 2))))

(defn primes-ugly []
  (letfn [(sieve [p v]
            (loop [primes p v v]
              (cond (empty? primes) (conj p v)
                    (zero? (mod v (first primes))) p
                    :otherwise (recur (rest primes) v))))
          (prime-list [] 
            (distinct (rest (reductions sieve [] (iterate inc 2)))))]
    (for [x (prime-list)] (last x))))

(defn fizzbuzz []
  (letfn [(div? [x & ys] (every? zero? (map #(mod x %) ys)))
          (fb [x] 
                (cond 
                  (div? x 3 5) (str x \: "fizzbuzz")
                  (div? x 3) (str x \: "fizz")
                  (div? x 5) (str x \: "buzz")
                  :else (str x)))]
    (map fb (range 1 101))))

(defn d [x & ys]
  (every? zero? (map #(mod x %) ys)))

;;4clojure #26
(defn fib [n]
  (loop [n (- n 2) [n0 n1 :as result] [1 1]]
    (if (<= n 0)
      (reverse result)
      (recur (dec n) (cons (+ n0 n1) result)))))
        
;;4clojure #26
(defn fib2 [n]
  (->> [1 1] 
    (iterate #(cons (+ (first %) (second %)) %))
    (take (dec n))
    last
    reverse))
        
;;4clojure #43
(defn rev-inter [xs n]
  (->> (interleave (flatten (repeat (range n))) xs)
    (partition 2)
    (group-by first)
    vals
    (map (partial map second))))

;;4clojure #44
(defn rot
  "rotate collection xs by n positions"
  [n xs]
  (let [n (->> xs count (mod n) inc)
        rot1 (fn [xs] (concat (rest xs) [(first xs)]))]
    (->> (iterate rot1 xs) (take n) last)))

;;4clojure #62
(defn iter
  "reimplement iterate"
  [f x] (cons x (lazy-seq (iter f (f x)))))

;;4clojure #62
;#(letfn [(iter [f x] (cons x (lazy-seq (iter f (f x)))))] 
;   (iter %1 %2))

;;4clojure #99
;;(fn [x y] (map (comp #(- % 48) int) (str (* x y))))

;;4clojure #63
(defn group [kfn c]
  (reduce (fn [m v]
            (update-in m [(kfn v)] (comp vec conj) v))
          {} c)) 

;;4clojure #122
(defn parse-bin [bits]
  (let [exp (range (count bits))
        two-nth #(reduce * 1 (repeat % 2))
        bit-val #(- (int %) 48)
        sum-bit (fn [total bit exp] (+ total (* (bit-val bit) (two-nth exp))))]
    (loop [[bit & bits] (reverse bits) [exp & exps] exp result 0]
      (if (empty? bits)
        (sum-bit result bit exp)
        (recur bits exps (sum-bit result bit exp))))))

(defn dot-product [c1 c2] 
  (->> 
    (interleave c1 c2)
    (partition 2)
    (map (partial apply *))
    (reduce +)))

(defn dot-product2 [c1 c2]
  (reduce + (map * c1 c2)))

;;4clojure #135
(defn calc [n1 op n2 & args]
  (let [total (op n1 n2) 
        n1 (first args) 
        n2 (second args)]
    (if (empty? args) 
      total 
      (recur total n1 n2 (drop 2 args)))))

;;4clojure #135
(defn calc2 [x op y & args]
  (let [total (op x y) [op n & more] args]
    (if (empty? args)
      total
      (recur total op n more))))

(defn row [n]
  (condp = n
    1 [1]
    2 [1 1]
    (map #(if (or (zero? %) (= (dec n) %)) 
            1 
            (let [v (row (dec n))]
              (+ (nth v %) (nth v (dec %)))))
         (range n))))

(defn foo [v]
  (let [inside (map (partial apply +) (partition 2 1 v))]    
    (vec (concat [1] inside [1]))))

(defn get-row [n]
  (last (take n (iterate foo [1]))))
    
(defn pascal [n]
  (let [row (fn [v]
              (let [inside (map (partial apply +) (partition 2 1 v))]    
                (vec (concat [1] inside [1]))))]
    (->> [1]
      (iterate row)
      (take n)
      last)))

;;4clojure #147
(defn pascal-trapezoid [c]
  (iterate (fn [v] (let [inside (map (partial apply +') (partition 2 1 v))]    
                     (vec (concat [(first v)] inside [(last v)]))))
           c))
                
(defn index-map [c]
  (map #(vector %1 %2) c (range (count c))))

(defn sum-sq-test [c]
  (let [sum-sq (fn [n] (apply + (map (comp #(* % %) #(- % 48) int) (str n))))]
    (count (filter #(< % (sum-sq %)) c))))
    
;;4clojure #95
(defn mirror-tree?
  ([[_ left right]] 
    (mirror-tree? left right))
  ([t1 t2]
    (or 
      (and (nil? t1) (nil? t2))
      (let [[v1 l1 r1] t1
            [v2 l2 r2] t2]
        (and (= v1 v2) 
             (mirror-tree? l1 r2)
             (mirror-tree? r1 l2))))))

(defn vijay-mirror [[x y z]]
  (letfn [(swap [[a b c]] 
            (conj [] a 
                  (if (vector? c) (swap c) c) 
                  (if (vector? b) (swap b) b)))] 
    (cond
      (and (vector? y) (vector? z)) (= y (swap z))
      (= y z) true 
      :else false)))

((fn [m] 
   (into '{} 
         (map (partial into {}) 
              (for [x m] 
               (let [k (key x) v (val x)]
                 (if (map? v) 
                   (map #(vector [k (key %)] (val %)) v)
                   (vector x))))))) '{a {p 1, q 2} b {m 3, n 4}})

;;4clojure #55
(fn [m] 
  (reduce 
    (fn [t v] 
      (update-in t [v] #(if (nil? %) 1 (inc %)))) {} m))

;;4clojure #56 wrong!!! not sorted
(comp vec (partial into (sorted-set)))

;;try again
  (#(reduce (fn [t v] (if (some #{v} t) t (conj t v))) [] %) [1 2 1 3 1 2 4])

(defn card [[s r]] 
  (let [rank (into {} (map vector [\2 \3 \4 \5 \6 \7 \8 \9 \T \J \Q \K \A] (range 13)))
        suit (into {} (map vector "SCDH" [:spade :club :diamond :heart]))]
    {:suit (get suit s) :rank (get rank r)}))

(defn lcm [& args0]
  (loop [args args0]
    (println args)
    (let [smallest (apply min args)]
      (if (apply = args)
        smallest
        (recur (map 
                 #(if (= %1 smallest) (+ %1 %2) %1) 
                 args args0))))))
    
;;4clojure #58
(defn mycomp [& fns]
  (fn [& args]
    (loop [fns (reverse fns) args args]
      (if (empty? fns)
        (first args)
        (let [new-arg (vector (apply (first fns) args))]
          (recur (rest fns) new-arg))))))

;;4clojure #59
(defn myapply [& fns]
  (fn [& args] (reduce (fn [t v] (conj t (apply v args))) [] fns)))

(defn mypart [n c]
  (loop [c c acc []]
    (if (< (count c) n)
      acc
      (recur (drop n c) (conj acc (take n c))))))

;;4clojure #67
#(take % (letfn [(sieve [p v]
                   (loop [primes p v v]
                     (cond (empty? primes) (conj p v)
                           (zero? (mod v (first primes))) p
                           :otherwise (recur (rest primes) v))))
                 (prime-list [] 
                   (distinct (rest (reductions sieve [] (drop 2 (range))))))]
           (for [x (prime-list)] (last x))))
  
(defn primes []
  (letfn [(sieve [p v]
            (loop [primes p v v]
              (cond (empty? primes) (conj p v)
                    (zero? (mod v (first primes))) p
                    :otherwise (recur (rest primes) v))))
          (prime-list [] 
            (distinct (rest (reductions sieve [] (drop 2 (range))))))]
    (for [x (prime-list)] (last x))))

(defn perfect-sq [n]
  (let [sqroot (java.lang.Math/sqrt n)]
    (= sqroot (java.lang.Math/floor sqroot))))

;;4clojure #74
(defn fps [s]
  (let [perfect #(= (java.lang.Math/sqrt %) 
                    (java.lang.Math/floor (java.lang.Math/sqrt %)))]
    (->> 
      (clojure.string/split s #",")
      (map read-string)
      (filter perfect)
      (clojure.string/join \,))))

;;4clojure #65
;;associative: [] or {}
(defn get-type [c]
  (cond 
    (associative? c) (if (= (into (empty c) {0 1}) {0 1})
                       :map
                       :vector)
    (> (count (conj c :a :a)) (count (conj c :a))) :list
    :else :set))

;;4clojure #80
(defn perfect? [n]
  (let [divs (for [x (range 1 (inc (/ n 2))) :when (zero? (mod n x))] x)]
    (= (apply + divs) n)))

;;4clojure #77
(defn anafind [c]
  (let [mk-key (comp (partial apply str) sort)
        list-add (fn [t v] (update-in t [(mk-key v)] (partial cons v)))]
    (->>
      (reduce list-add {} c)
      vals
      (map set)
      (filter #(> (count %) 1))
      (into #{}))))

;;4clojure #60
(defn myreduce
  ([f coll]
    (myreduce f (first coll) (rest coll)))
  ([f total coll]
    (if (empty? coll)
      [total]
      (let [newtotal (f total (first coll))]
        (cons total (lazy-seq (myreduce f newtotal (rest coll))))))))

;;4clojure #69
(defn mymw [f m & ms]
  (if (empty? ms)
    m
    (recur f 
           (into m (for [el (first ms)]
                     (let [[k v] el cur (get m k)]
                       (if (nil? cur)
                         el
                         [k (f cur v)]))))
           (rest ms))))

;;4clojure #73
;;when all else fails, use brute force...
(defn winner [xs]
  (letfn [(w ([a b c] (when (and (not= :e a) (= a b c)) a))
             ([[a b c]] (w a b c)))]
    (some #(and (not= :e %) %)
          (conj
            (map w xs)
            (w (first (first xs)) (first (second xs)) (first (last xs)))
            (w (second (first xs)) (second (second xs)) (second (last xs)))
            (w (last (first xs)) (last (second xs)) (last (last xs)))
            (w (first (first xs)) (second (second xs)) (last (last xs)))
            (w (last (first xs)) (second (second xs)) (first (last xs)))))))
              
(= nil (winner [[:e :e :e]
                [:e :e :e]
                [:e :e :e]]))

(= :x (winner [[:x :e :o]
               [:x :e :e]
               [:x :e :o]]))

;;4clojure #102
#(reduce 
   (fn [t v] (apply str t (clojure.string/upper-case (first v)) (rest v))) 
   (clojure.string/split %1 #"-"))

;;4clojure #75
(defn totient [n]
  (if (= n 1) 1
    (count (letfn [(gcd [a b] (if (zero? b) a (recur b (mod a b))))]
             (for [x (range 1 n) :when (= 1 (gcd x n))] x)))))

;;4clojure #86
(defn happy [x]
  (loop [x x tot []]
    (cond 
      (= x 1) true
      (some #{x} tot) false
      :otherwise (let [m (apply + (map #(let [n (- (int %) 48)] (* n n)) 
                               (str x)))]
                   (recur m (conj tot x))))))

;;4clojure #78
(defn tramp [f & args]
  (loop [f (apply f args)]
    (if (fn? f) (recur (f)) f)))
  
;;4clojure 115
(defn balanced [x]
  (let [s (map #(- (int %) (int \0)) (str x))
        len (int (/ (count s) 2))
        sum #(->> % (take len) (reduce +))]
    (= (sum s) (sum (reverse s)))))
    
;;4clojure 85
(defn power [s]
  (letfn [(f [e t] (reduce #(conj %1 (conj %2 e)) #{} t))]
    (if (empty? s) #{s}
      (let [e (first s) t (disj s e)]
        (clojure.set/union (power t) (f e (power t)))))))

;;4clojure #53
(defn prob53 [[f & c]]
  (let [sseq (fn [t v] (if (= v (inc (last t))) (conj t v) [v]))
        newc (reduce 
               #(if (= (count %1) (count %2)) %2 %1) 
               (reverse (sort-by count (reductions sseq [f] c))))]
    (if (> (count newc) 1) newc [])))

;;4clojure #105
  (defn prob105 [s]
  (loop [m {} k nil [fs & s] s]
    (cond
      (nil? fs) m
      (keyword? fs) (recur (assoc m fs []) fs s)
      :else (recur (update-in m [k] conj fs) k s))))

;;4clojure #137
(defn prob137 [num base]
  (loop [num num result '()]
    (if (< num base)
      (conj result num)
      (recur (quot num base) (conj result (mod num base))))))

;;4clojure #110
(defn prob110 [s]
  (rest (letfn [(pronounce [s] 
                  (->> s
                    (partition-by identity)
                    (map (fn [ns] [(count ns) (first ns)]))
                    flatten))]
          (iterate pronounce s))))

;;4clojure #144
(defn prob144 [x & fns]
  (let [fns (cycle fns)]
    (reductions (fn [t v] (v t)) x fns)))

;;4clojure #93
(defn prob93 [xs]
  (filter
    #(and (coll? %) (not (coll? (first %))))
    (tree-seq coll? identity xs)))

(defn p93 [xs]
  (filter
    #(and (coll? %) (every? (comp not coll?) %))
    (tree-seq coll? identity xs)))

;;4clojure #132
(defn prob132 [p? word xs]
  (mapcat 
    (fn [[f s]] 
      (if (or (nil? s) (not (p? f s))) [f] [f word]))
    (partition-all 2 1 xs)))

;;4clojure #116
(defn balanced? [n]
  (letfn [(sieve [p v]
            (loop [primes p v v]
              (cond (empty? primes) (conj p v)
                    (zero? (mod v (first primes))) p
                    :otherwise (recur (rest primes) v))))
          (prime-list [] 
            (distinct (rest (reductions sieve [] (drop 2 (range))))))
          (primes [] (for [x (prime-list)] (last x)))]
    (= n (last (take-while 
                 #(<= % n) 
                 (for [[p1 p2 p3] (partition 3 1 (primes)) 
                       :when (= p2 (/ (+ p1 p3) 2))] p2))))))

;;4clojure #150
;;still timing out on one test
(defn pal [n]
  (letfn [(pal? [n] 
            (= n (loop [n n total 0]
                   (if (zero? n) 
                     total
                     (recur (quot n 10) 
                            (+ (* 10 total) (mod n 10)))))))]
    (for [x (iterate inc n) :when (pal? x)] x)))

;;this works but yuck! kind of cheating with the qs vector. needs lazy sequence
(defn pal
  ([n] (pal n true))
  ([n must-check]
    (letfn [(pal? [n] 
              (= n (loop [n n total 0]
                     (if (zero? n) 
                       total
                       (recur (quot n 10) 
                              (+ (* 10 total) (mod n 10)))))))
            (first-pal [n]
              (first (drop-while #(not (pal? %)) (iterate inc n))))]
      (if must-check
        (pal (first-pal n) false)
        (let [qs [1 2 10 11 100 110 1000 1100 10000 11000 100000 110000]]
          (cons n (lazy-seq (pal 
                              (+ n (first (drop-while #(not (pal? (+ n %))) qs))) 
                              false))))))))

;;4clojure #158
;;best
(fn [f] 
  (fn [& args]
    (reduce (fn [t v] (t v)) f args)))

;;first attempt
(defn foo [f] 
  (fn [& args]
    (loop [f f args args]
      (if (= (count args) 1)
        (f (first args))
        (recur (f (first args)) (rest args))))))

;;4clojure #114
(fn nf [n pred xs]
  (if (or (zero? n) 
          (empty? xs)
          (and (= 1 n) (pred (first xs))))
    nil
    (cons (first xs)
          (lazy-seq (nf (if (pred (first xs)) (dec n) n) 
                        pred 
                        (rest xs))))))

;;4clojure #177
(defn parse [xs]
  (letfn [(match [x y]
            (= y ({\} \{ \] \[ \) \(} x)))]
    (loop [[x & xs] xs prevs '() n 0]
      (cond 
        (< n 0) false
        (some #{\{\[\(} [x]) (recur xs (cons x prevs) (inc n))
        (some #{\}\]\)} [x]) (if (match x (first prevs))
                               (recur xs (drop 1 prevs) (dec n))
                               false)
        (empty? xs) (zero? n)
        :else (recur xs prevs n)))))

;;4clojure 108
(defn prob108 [& xs]
  (let [biggest (apply max (map #(first %) xs))
        drop-less (fn [x xs] (drop-while #(< % x) xs))]
    (cond
      (some empty? xs) nil
      (apply = (map #(first %) xs)) biggest
      :else (recur (for [x xs] (drop-less biggest x))))))

;;4clojure 82 =======================================================
(defn chain? [words]
  (letfn [(trans [xs ys]
            (loop [x (seq xs) y (seq ys)]
              (cond 
                (= x y) ys
                (= (next x) y) ys
                (= x (next y)) ys
                (= (next x) (next y)) ys
                (= (butlast x) y) ys
                (= x (butlast y)) ys
                (= (first x) (first y)) (recur (next x) (next y)))))
          (edges [ws]
            (for [x ws y ws :when (and (not= x y) (trans x y))] [x y]))
          (connected [word edges]
            (map last (filter #(= word (first %)) edges)))
          (grow [ws]
            (filter (partial apply distinct?)
                    (map (partial conj ws) 
                         (connected (last ws) (edges words)))))]
    (boolean (not-empty 
               (last (take (count words) 
                           (iterate (partial mapcat grow) 
                                    (map vector words))))))))

;;euler.net #4
(last 
  (sort
    (for [x (range 100 1000) y (range x 1000) 
          :when (let [n (str (* x y))] (= (seq n) (reverse n)))]
      [(* x y) x y])))

(apply max 
       (for [x (range 100 1000) y (range x 1000)
             :let [n (* x y) s (str n)]
             :when (= (seq s) (reverse s))]
         n))

(defn swap-sub [[x y]]
  [(dec y) x])

;(take 20 (iterate swap-sub [999 999]))

;;4clojure #83 ugly reduce solution
(defn some-true [& bs]
  (reduce (fn [t v]) bs))

(defn some-true [& bs]
  (let [acc (reduce (fn [t v] (if v 
                                (assoc t :result true)
                                (assoc t :false true)))
                      {:result false :false false}
                      bs)]
    (and (:result acc) (:false acc))))

;;4clojure #83 pretty solution
(defn some-true [& bs]
  (apply not= bs))

;;#128 Recognize Playing Cards
(defn card [[s r]] 
  (let [rank (zipmap [\2 \3 \4 \5 \6 \7 \8 \9 \T \J \Q \K \A] (range 13))
        suit (zipmap "SCDH" [:spade :club :diamond :heart])]
    {:suit (get suit s) :rank (get rank r)}))

(= (range 13) (map (comp :rank card str)
                   '[S2 S3 S4 S5 S6 S7
                     S8 S9 ST SJ SQ SK SA]))

;;#100 Least Common Multiple
(defn lcm [& xs]
  (loop [mults (map #(for [x (drop 1 (range))] (* x %)) xs)]
    (let [fs (map first mults)]
      (if (apply = fs)
        (first fs)
        (let [smallest (apply min fs)]
          (recur (map #(if (= smallest (first %)) (rest %) %) mults)))))))

;;#171 Intervals
(defn g [acc v]
  (if (and (not-empty acc) (< (Math/abs (- (ffirst acc) v)) 2)) 
    (cons (cons v (first acc)) (rest acc))
    (cons [v] acc)))

(defn intervals [xs]
    (letfn [(f [acc v] 
              (if (and (not-empty acc) (< (Math/abs (- (ffirst acc) v)) 2))
              (cons (cons v (first acc)) (rest acc))
              (cons [v] acc)))]
      (sort-by first (map #(vector (apply min %) (apply max %)) 
                       (reduce f [] (sort xs))))))

  (= (intervals [10 9 8 1 2 3]) [[1 3] [8 10]])

;;#112 Sequs Horribilis
(defn max-seq [n xs]
  (letfn [(f [[x & xs] tot acc]
              (if (sequential? x)
                (conj acc (f x tot []))
                (if (or (nil? x) (> (+ tot x) n))
                  acc
                  (recur xs (+ tot x) (conj acc x)))))]
    (f xs 0 [])))

;;#103 Generating k-combinations
;;reuse the powerset solution
(defn k-comb [n s]
  (letfn [(power [s] (letfn [(f [e t] (reduce #(conj %1 (conj %2 e)) #{} t))]
                       (if (empty? s) #{s}
                         (let [e (first s) t (disj s e)]
                           (clojure.set/union (power t) (f e (power t)))))))]
    (into #{} (filter #(= n (count %)) (power s)))))
 
 
;The Big Divide #148
;too slow
;(defn p148 [n a b]
;  (let [multiples (map #(iterate (partial +' %) %) [a b])]
;    (for [l multiples] (take-while #(< % n) l))))
    
(defn p148-0 [n a b]
  (let [f (fn [x] (int (/ (dec n) x)))]
    (apply + (distinct 
               (concat (take (f a) (iterate (partial + a) a))
                       (take (f b) (iterate (partial + b) b)))))))

(defn p148 [n a b]
  (let [f (fn [x] (bigint (/ (dec n) x)))]
    (apply +' (concat 
               (filter #(not= 0 (mod % b)) (take (f a) (iterate (partial +' a) a)))
               (take (f b) (iterate (partial +' b) b))))))

(defn p148-fast [n a b]
  (let [f #(bigint (/ (dec n) %))
        v #(vector (+' (first %) 
                      (*' a (second %))) 
                   (+' 1 (second %)))]
    (take (f a) (iterate v [a 2]))))
  
(= 233168 (p148 1000 3 5))

;#104 Write Roman Numerals
(defn roman [n]
  (let [m {1 \I 5 \V 10 \X 50 \L 100 \C 500 \D 1000 \M}]
    (m n)))
