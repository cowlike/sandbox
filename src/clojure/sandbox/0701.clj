(ns sandbox.0701)

(apply not= [1 2 3])
(= 1)
(= false)

(defn f [x y] (prn x) y)

;;sherry's: fixed version of what we were trying in class
(defn some-true? [& bs]
  (loop [bs bs t? 1 f? 1]
    (cond 
      (and (not= t? f? 1) (= t? f?)) true
      (empty? bs) (not (or (= t? 1) (= f? 1)))
      (first bs) (recur (rest bs) true f?)
      :else (recur (rest bs) t? true))))

(defn some-true? [& bs]
  (loop [[b & bs] bs t? 1 f? 1]
    (cond 
      (and (not= t? f? 1) (= t? f?)) true
      (nil? b) (not (or (= t? 1) (= f? 1)))
      b (recur bs true f?)
      :else (recur bs t? true))))


;;john's: just implement (not=)
(defn some-true? [b & bs]
  (loop [bs bs]
    (cond 
      (empty? bs) false
      (not= (first bs) b) true
      :else (recur (rest bs)))))

;;vijay's: uses a set
(defn some-true? [& bs]
  (loop [bs bs acc #{}]
    (if (= (count acc) 2)
      true
      (if (empty? bs)
        false
        (recur (rest bs) (conj acc (first bs)))))))

(defn zm [ks vs]
  (apply hash-map (interleave ks vs)))

(= (zm [:a :b :c] [1 2 3]) {:a 1, :b 2, :c 3})