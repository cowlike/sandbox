(ns sandbox.mytest
  (require [clojure.data.json :as json])
  (:import [java.io StringReader]))

(fn [n] (inc n))

#(inc %)

(fn add-five [x] 
  (+ x 5))

(defn positive-numbers 
	([] (positive-numbers 1))
	([n] (cons n (lazy-seq (positive-numbers (inc n))))))
   

(defn smap [c total] 
  (if (empty? c) 
    total 
    (recur (rest c) (conj total (* 2 (first c))))))

(smap [1 2 3 4] [])

(defn sfilter  [f c total]
  (if (empty? c)
    total
    (recur f 
           (rest c) 
           (if (f (first c)) 
             (conj total (first c))
             total))))

(defn sfilter [f c]
  (loop [f f [c & more] c total []]
    (if (nil? c)
      total
      (recur f more (if (f c) 
                      (conj total c) 
                      total)))))
 
(sfilter #(> % 2) [1 5 2 8])
(sfilter #(even? %) [2 2 3 8])
(sfilter #(contains? (set "aeiouAEIOU") %) "AaardvarUk")

(defn rev [s]  
  (prn s)
  (if (empty? s)
    s
    (conj (vec (rev (rest s))) (first s))))

(defn mymap [f [fst & rst]]
  (if (nil? fst) fst
    (conj (mymap f rst) (f fst))))

(defn mymap2 [f [fst & rst]]
  (when-not (nil? fst)
    (cons (f fst) (lazy-seq (mymap2 f rst)))))

(defn mymap-t [f c]
  (loop [f f c c total []]
    (if (empty? c) total
      (recur f (rest c) (conj total (f (first c)))))))

;[{"Name":"ADM.cocom"},{"Name":"RDS"},{"Name":"Regional Carriers"},{"Name":"Financial Systems"},{"Name":"ADM.PFSE"},{"Name":"ADM.CommercialPortfolio"},{"Name":"Crew-IT"},{"Name":"Check In"},{"Name":"ADM.Airframe"},{"Name":"Business Services"},{"Name":"Host Services"},{"Name":"CTI"},{"Name":"ADM.CompSvcs"},{"Name":"Marketing"},{"Name":"Food Services"},{"Name":"Agent GUI"},{"Name":"Project Management Office"},{"Name":"Airport Services"},{"Name":"DBA"},{"Name":"Human Resources"},{"Name":"Tealeaf"},{"Name":"Field Services"},{"Name":"ADM Projects"},{"Name":"Safety and Security"},{"Name":"Application Ops"},{"Name":"PFSE"},{"Name":"ADM.ScheduleEngine"},{"Name":"Reservations"},{"Name":"ADM.CCP"},{"Name":"ADM.ContinentalDotCom"},{"Name":"Human Resources Testing 2"},{"Name":"Sales"},{"Name":"Aero UI framework"},{"Name":"Concur Open Booking"},{"Name":"Enterprise Engineering"},{"Name":"RampMobile"},{"Name":"Turn Manager"},{"Name":"Tech Ops IT Testing"},{"Name":"EQA Automation"},{"Name":"Technology"},{"Name":"Host Systems"},{"Name":"Common Services"},{"Name":"ADM.Sense"},{"Name":"eCommerce"},{"Name":"Cargo"},{"Name":"Loyalty"},{"Name":"Merchandising"},{"Name":"EDT Team"},{"Name":"Flight Operations"},{"Name":"ADM.ATRE"},{"Name":"Data Warehouse"},{"Name":"CustomBuildSettings"},{"Name":"Platform Automation"},{"Name":"Sales and Distribution"},{"Name":"RI"},{"Name":"Eco Task"},{"Name":"PRM"},{"Name":"Technical Operations"},{"Name":"Flights and Fares"},{"Name":"CCP proof of concept"},{"Name":"Human Resources Testing"},{"Name":"Food Service"},{"Name":"ADM.ETA"}]
;[{"Key":"Project Administrators","Value":[{"Email":"Robert.Lamberson@united.com","Name":"Lamberson, Robert"},{"Email":"Aaron.Hynes@united.com","Name":"Hynes, Aaron"},{"Email":"ferdy.khater@united.com","Name":"Khater, Ferdy"},{"Email":"michael.mcdonald@united.com","Name":"McDonald, Michael"}]},{"Key":"Project Valid Users","Value":[{"Email":"nancy.lin@united.com","Name":"Lin, Nancy"},{"Email":"Robert.Lamberson@united.com","Name":"Lamberson, Robert"},{"Email":"Aaron.Hynes@united.com","Name":"Hynes, Aaron"},{"Email":"adam.rasmusson@united.com","Name":"Rasmusson, Adam"},{"Email":"khon.truong@united.com","Name":"Truong, Khon"},{"Email":"ferdy.khater@united.com","Name":"Khater, Ferdy"},{"Email":"larry.robinson@united.com","Name":"Robinson, Lawrence"},{"Email":"Derek.Smith@united.usa.net","Name":"Smith, Derek"},{"Email":"jie.luo@united.com","Name":"Luo, Jie"},{"Email":"ray.lam@united.com","Name":"Lam, Raymond"},{"Email":"srividya.perumaalla@united.com","Name":"Perumaalla, Srividya"},{"Email":"john.petraitis@united.com","Name":"Petraitis, John"},{"Email":"christoph_schrey@mckinsey.com","Name":"Schrey, Christoph"},{"Email":"ashish.a.panchal@united.com","Name":"Panchal, Ashish"},{"Email":"","Name":"svc-tfs-reader"},{"Email":"christopher.moten@united.com","Name":"Moten, Christopher"},{"Email":"donna.lewis@united.com","Name":"Lewis, Donna"},{"Email":"","Name":"svc-TFS-sqlagent"},{"Email":"angelica.zdanewicz@united.com","Name":"Zdanewicz, Angelica"},{"Email":"beth.labelle@united.com","Name":"LaBelle, Beth"},{"Email":"","Name":"Kuppuswamy, Shiva"},{"Email":"Igor.Hoffman@united.com","Name":"Hoffman, Igor"},{"Email":"ben.vaughn@united.com","Name":"Vaughn, Ben"},{"Email":"ramana.chamarti@united.com","Name":"Chamarti, Ramana"},{"Email":"jean.miller@united.com","Name":"Miller, Jean"},{"Email":"hai.hoang@united.com","Name":"Hoang, Hai"},{"Email":"conrad.houeto@united.com","Name":"Houeto, Conrad"},{"Email":"michael.mcdonald@united.com","Name":"McDonald, Michael"},{"Email":"nicholas.stuberg@united.com","Name":"Stuberg, Nicholas"},{"Email":"quoc.tran@united.com","Name":"Tran, Quoc"},{"Email":"geraldine.tomas@united.com","Name":"Tomas, Geraldine"},{"Email":"moni.mau@united.com","Name":"Mau, Moni"},{"Email":"hao.zhou@united.com","Name":"Zhou, Hao"}]},{"Key":"Build Services","Value":[]}]

(def sentences ["Mary read a story to Sam and Isla." 
                "Isla cuddled Sam." 
                "Sam chortled."])
(reduce #(+ %1 (count (re-seq #"Sam" %2))) 0 sentences)

