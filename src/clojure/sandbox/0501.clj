(ns sandbox.0501)

(for [a (range 1 50)] 
  (cond
    (and (= (mod a 3) 0) (= (mod a 5) 0)) (println (str a "-FizzBuzz"))
    (= (mod a 3) 0) (println (str a "-Fizz"))
    (= (mod a 5) 0) (println (str a "-Buzz"))
  :else a))


(let [x 10]
  (cond (false? x) 1
        (true? x) 2
        :otherwise "foo!"))

(defn fb [xs]
  (for [x xs] 
    (cond
      (and (zero? (mod x 3)) (zero? (mod x 5))) (str x "-FizzBuzz")
      (zero? (mod x 3)) (str x "-Fizz")
      (zero? (mod x 5)) (str x "-Buzz")
      :else (str x))))

(doseq [s (fb (range 1 51))] (println s))

(->> (for [x (range) :when (odd? x)] (* x x))
     (drop 998)
     first)

(for [x [:a :b :c] y [0 1]]
  [x y])

(defn foo []
  (let [oddsquares (for [x (range) :when (odd? x)] (do (prn x "!") (* x x)))]
    (take 17 oddsquares)))
  