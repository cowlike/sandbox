(ns sandbox.stuff)

(defn foo [a b]
  (println a))

((fn [a b]
   (vector b a)) "one" "two")

(#(vector %2 %1) 1 2)

(defn greet [x]
  (str "Hello, " x "!"))

(= (greet "Dave") "Hello, Dave!")

(defn nums [n]
  (cons n (lazy-seq (foo (inc n)))))

;;============== base conversion ===============
(def digits "0123456789abcdef")

(defn cvt [n radix]
  (loop [n n acc '()]
    (if (< n radix)
      (apply str (cons (nth digits n) acc))
      (recur (quot n radix) (cons (nth digits (mod n radix)) acc)))))

(defn cvt [n radix]
  (letfn [(f [x xs] (cons (nth digits x) xs))]
    (loop [n n acc '()]
      (if (< n radix)
        (apply str (f n acc))
        (recur (quot n radix) (f (mod n radix) acc))))))

(defn cvt [n radix]
  (if (< n radix)
    (vector (nth digits n))
    (let [lst (cvt (quot n radix) radix)]
      (conj lst (nth digits (mod n radix))))))

;;====================================
(defn fizbuzz [xs]
  (letfn [(m3? [n] (zero? (mod n 3)))
          (m5? [n] (zero? (mod n 5)))]
    (for [x xs]
      (cond (and (m3? x) (m5? x)) "fizzbuzz"
            (m3? x) "fizz"
            (m5? x) "buzz"
            :otherwise x))))

(defn digits [n]
  (if (zero? n) []
    (cons (mod n 10) (digits (quot n 10)))))

(defn sum-sq [xs]
  (reduce (fn [t v] (+ t (* v v))) 0 xs))

(defn happy? [n]
  (letfn [(digits [n]
            (if (zero? n) 
              []
              (cons (mod n 10) (digits (quot n 10)))))
          (sum-sq [xs]
            (reduce (fn [t v] (+ t (* v v))) 0 xs))]
    (loop [n n acc []]
      (let [h (sum-sq (digits n))]
        (cond 
          (= 1 h) true
          (some (partial = n) acc) false
          :otherwise (recur h (conj acc n))))))) 

(def happy (for [x (range) :when (happy? x)] x))

(defn happy-test [xs]
  (distinct (lazy-seq (cons (some #(when (happy? %) %) xs) (happy-test (rest xs))))))

(defn rev-digits [x]
  (loop [x x acc 0]
    (let [div (unchecked-divide-int x 10)]
      (if (zero? div)
        (+ x (* acc 10))
        (recur div (+ (* acc 10) (mod x 10)))))))

(defn length [[x & xs]]
  (if (nil? x) 
    0 
    (+ 1 (length xs))))

;;codewars

(defn seqlist
  "Generates consecutive terms in a sequence"
  [first c terms]
  (take terms (iterate (partial + c) first)))

(defn power [s]
  (letfn [(f [e t] (reduce #(conj %1 (conj %2 e)) [] t))]
    (if (empty? s) [s]
      (let [e (first s) t (remove #(= e %) s)]
        (concat (power t) (f e (power t)))))))

(defn nth-term
  [x idx c]
  (first (drop idx (iterate (partial + c) x))))

(defn reverse_
  "Reverse a list"
  [lst]
  (reduce conj '() lst))

(defn fac [n] (if (< n 2) 1 (reduce *' (range 2 (inc n)))))

(defn powers [s]
  (letfn [(fac [n] (if (< n 2) 1 (reduce *' (range 2 (inc n))))) 
          (combos [n k] (/ (fac n) (* (fac k) (fac (- n k)))))]
    (inc (apply + (for [n (range 1 (inc (count s)))] (combos (count s) n))))))

(defn powers [s]
  (let [fac (fn [n] (if (< n 2) 1 (reduce *' (range 2 (inc n))))) 
        combos (fn [n k] (/ (fac n) (* (fac k) (fac (- n k)))))
        num (count s)]
    (inc (apply + (for [n (range 1 (inc num))] (combos num n))))))

(def head first)
(def tail rest)
(def init (comp reverse first reverse))
(def last_ (comp first reverse))

(defn reverseLonger [a b]
  (let [[a b] (sort-by count [b a])]
    (apply str (concat a (reverse b) a))))

(defn element-at
  "Find the K'th element of an ISeq"
  [lst n]
  (nth lst (dec n)))

(defn round [s n] 
  (.setScale (bigdec n) s java.math.RoundingMode/HALF_EVEN))

(defn piseq
  "Implements the Leibniz formula for calculating on pi. Generates a lazy 
  series that converges on pi: 1 - 1/3 + 1/5 - 1/7 + 1/9 - ... = pi/4"
  []
  (letfn [(mkseq [n d] 
            (lazy-seq (cons (/ (first n) (first d)) 
                            (mkseq (rest n) (rest d)))))]
    (mkseq (iterate - 4) (iterate (partial + 2) 1))))

;;#128 Recognize Playing Cards
(defn card [[s r]] 
  (let [rank (zipmap [\2 \3 \4 \5 \6 \7 \8 \9 \T \J \Q \K \A] (range 13))
        suit (zipmap "SCDH" [:spade :club :diamond :heart])]
    {:suit (get suit s) :rank (get rank r)}))

(= (range 13) (map (comp :rank card str)
                   '[S2 S3 S4 S5 S6 S7
                     S8 S9 ST SJ SQ SK SA]))

;;#146 Trees into tables
(defn mflat [m]
  (into {} (for [x (keys m) y (keys (m x))] 
             [[x y] (get-in m [x y])])))

(= (mflat '{a {p 1, q 2}
         b {m 3, n 4}})
   '{[a p] 1, [a q] 2
     [b m] 3, [b n] 4}) 

;;#153 Pairwise Disjoint Sets
;;simple solution
(defn disj1 [s]
  (= (count (reduce into s))
     (reduce + (map count s))))

;;complicated solution
(defn disj1 [s]
  (let [r (rest s)]
    (if (nil? (first r))
      true
      (if (every? empty? (for [x r] (clojure.set/intersection (first s) x)))
        (recur r)
        false))))
            
(= (disj1 #{#{\U} #{\s} #{\e \R \E} #{\P \L} #{\.}})
      true)

(= (disj1 #{#{:a :b :c :d :e}
         #{:a :b :c :d}
         #{:a :b :c}
         #{:a :b}
         #{:a}})
   false)

(= (disj1 #{#{'(:x :y :z) '(:x :y) '(:z) '()}
         #{#{:x :y :z} #{:x :y} #{:z} #{}}
         #{'[:x :y :z] [:x :y] [:z] [] {}}})
   false)

;;#46 Flipping out
(defn flip [f]
  #(f %2 %1))  ; or (fn [x y] (f y x))

;;#44 Rotate Sequence
(defn rot [n xs]
  (let [n (mod n (count xs))]
    (concat (drop n xs) (take n xs))))
    
(= (rot 2 [1 2 3 4 5]) '(3 4 5 1 2))
(= (rot -2 [1 2 3 4 5]) '(4 5 1 2 3))

;;#60
(defn red 
  ([f [x & xs]] (red f x xs))
  ([f acc [x & xs]] (lazy-seq (cons acc (when x (red f (f acc x) xs))))))
  
(= (red conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])

;;#64
(defn mw [f & ms]
  (reduce (fn [t v]
            (reduce (fn [tt vv]
                      (if (nil? (tt vv)) 
                        (assoc tt vv (v vv)) 
                        (update-in tt [vv] f (v vv)))) 
                    t 
                    (keys v))) 
          ms))


(defn mw [f & ms]
  (->> ms
    (mapcat vec)
    (group-by first)
    (map #(vector (key %) 
                  (if (= 1 (count (val %)))
                    (second (first (val %)))
                    (apply f (map second (val %))))))
    (into {})))

(= (mw - {1 10, 2 20} {1 3, 2 10, 3 15})
   {1 7, 2 10, 3 15})

(= (mw * {:a 2, :b 3, :c 4} {:a 2} {:b 2} {:c 5})
   {:a 4, :b 6, :c 20})

;;#102
(defn camel [s]
  (clojure.string/replace s #"-[a-z]" #(clojure.string/upper-case (second %))))

(= (camel "something") "something")
(= (camel "multi-word-key") "multiWordKey")

;;#75
(defn totient [x]
  (letfn [(gcd [a b] (if (zero? b) a (recur b (mod a b))))]
    (if (= x 1) 1 (count (filter #(= 1 (gcd % x)) (range 1 x))))))

