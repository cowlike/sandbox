(ns sandbox.1030)

(defn red
  ([f xs] (red f (first xs) (rest xs)))
  ([f x xs]
      (lazy-seq (cons x
                      (when (not-empty xs)
                        (red f (f x (first xs)) (rest xs)))))))

(= (take 5 (red + (range))) [0 1 3 6 10])

(= (red conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])