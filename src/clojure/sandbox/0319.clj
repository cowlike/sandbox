(ns sandbox.0319)

(defn add [c]
  (if (empty? c)
    0
    (+ (first c) (add (rest c)))))

;;bad! not tail recursive
;(defn add-recur [c]
;  (if (empty? c)
;    0
;    (+ (first c) (recur (rest c)))))

(defn add-recur-bogus [c]
  (if (empty? c)
    0
    (recur (rest c))))

(defn add-t [c total]
  (if (empty? c)
    total
    (recur (rest c) (+ total (first c))))) 

(defn add-loop [c]
  (loop [total 0 c c]
    (if (empty? c)
      total
      (recur (+ total (first c)) (rest c)))))

;;broken
;(defn add-loop [c]
;  (let [total 0]
;    (if (empty? c)
;      total
;      (recur (+ total (first c)) (rest c)))))

;;multi-arity
(defn add-multi
  ([c] "bob")
  ([c total]
    (if (empty? c)
      total
      (recur (rest c) (+ total (first c))))))
       
(def add-multi2 
  (fn 
    ([c] "bob") 
    ([c total] (if (empty? c) total (recur (rest c) (+ total (first c)))))))

(defn add-bob [c] "bob")
(defn add-real [c total]
  (if (empty? c) 
    total 
    (recur (rest c) (+ total (first c)))))

(defn add-multi-x
  ([x] (add-bob x))
  ([x y] (add-real x y)))

;;multi-arity with local function definition
(defn add-local-bob
  ([x]
    (let [x2 (str x "abc")
          bob (fn [n] (str x2 "bob"))]
      (bob x)))
  ([x y] (add-real x y)))