(ns sandbox.0922)

(defn rotate [n xs]
  (let [c (count xs)
        n' (cond 
             (neg? n) (+ c n)
             (> n c) (-)
             :else n)]
    (concat (drop n' xs) (take n' xs))))

(defn rotate [n xs]
  (let [c (mod n (count xs))]
    (concat (drop c xs) (take c xs))))

(= (rotate 2 [1 2 3 4 5]) '(3 4 5 1 2))

(= (rotate -2 [1 2 3 4 5]) '(4 5 1 2 3))

(= (rotate 6 [1 2 3 4 5]) '(2 3 4 5 1))

(= (rotate -6 [1 2 3 4 5]) '(5 1 2 3 4))