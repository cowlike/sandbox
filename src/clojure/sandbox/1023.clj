(ns sandbox.1023)

(defn perf-sq [s]
  (let [xs (map read-string (clojure.string/split s #","))
        pred #(= (Math/sqrt %) (Math/floor (Math/sqrt %)))]
    (->> xs
      (filter pred)
      (clojure.string/join ","))))

(= (perf-sq "4,5,6,7,8,9") "4,9")

(defn div [x]
  (apply + (filter #(zero? (mod x %)) (range 1 x))))

(defn perfect? [x]
  (= x 
     (apply + (filter #(zero? (mod x %)) (range 1 x)))))

(= (perfect? 8128) true)

(= (perfect? 500) false)

(defn anagrams [xs]
  (->> xs
    (group-by sort)
    vals
    (filter #(> (count %) 1))
    (map set)
    (into #{})))   

(= (anagrams ["veer" "lake" "item" "kale" "mite" "ever"])
   #{#{"veer" "ever"} #{"lake" "kale"} #{"mite" "item"}})