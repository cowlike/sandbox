(ns sandbox.0626)

(= [2 4] (let [[a b c d e f g] (range)] [c e]))

(defn split [n xs]
  (loop [acc [] n n xs xs]
    (if (zero? n) 
      (conj [] acc xs)
      (recur (conj acc (first xs)) (dec n) (rest xs)))))
  
(= (split 3 [1 2 3 4 5 6]) [[1 2 3] [4 5 6]])

(defn split2 [n xs]
  (conj [] (take n xs) (drop n xs)))

(if false (+ 1 2) "foo")

;(= [1 2 [3 4 5] [1 2 3 4 5]] (let [[a b & c :as d] __] [a b c d]))

(let [[a b & c :as d]  [1 2 3 4 5]] [a b c d])
(let [[x _ y]  [1 2 3 4 5]] [x y])

;;4clojure 83
(defn some-true? [& bs]
  (not (apply = bs)))

(defn some-true? [& bs]
  (let [f (fn [acc v] (if v 
                        (assoc acc :got-true true)
                        (assoc acc :got-false true)))
        result (reduce f {:got-true false :got-false false} bs)]
    (and (:got-true result) (:got-false result))))