(ns sandbox.1202)

(defn merge-fn [f & xs]
  (reduce (fn [acc x] 
            (reduce-kv (fn [m k v] 
                         (prn m)
                         (assoc m k (if (contains? m k) 
                                      (f (m k) v) 
                                      v))) 
                       acc x))
          xs))


(= (merge-fn * {:a 2, :b 3, :c 4} {:a 2} {:b 2} {:c 5})
   {:a 4, :b 6, :c 20})

(= (merge-fn > {:a 2, :b 3, :c 4} {:a 2} {:b 2} {:c 5})
   {:a 4, :b 6, :c 20})

