(ns sandbox.file
  (:require [clojure.java.io :refer [reader writer]]
            [clojure.string :refer [split lower-case] :as str]))

(defn get-data
  "Load file data info a vector of lines"
  [filename]
  (with-open [rdr (reader filename)]
    (into [] (line-seq rdr))))

(defn csv->list
  "Create list of maps from csv file where first line has the keys"
  [filename]
  (let [lines (get-data filename)
        ks (->> lines 
             ((comp #(split % #",") 
                    #(str/replace % " " "_") 
                    lower-case 
                    first)) 
             (map keyword))
        lines (rest lines)]
    (for [line lines]
      (let [words (split line #",")]
        (zipmap ks words)))))

(defn process-file
  "Process a text file by calling function f on a lazy sequence of
   all the lines in the file.
   Some sample usage:
     (process-file count \"/log.txt\")
     (process-file last \"/log.txt\")
     (process-file (fn [lines] (doseq [l lines] (println l))) \"/log.txt\")
     (process-file #(doall (partition 2 %)) \"/log.txt\")"
  [f fname]
  (with-open [rdr (reader fname)]
    (f (line-seq rdr))))

(defn find-pattern
  "Find instances of a a regular expression pattern in a file."
  [fname pattern]
  (let [pred (partial re-find pattern)]
    (process-file 
      (fn [c] (doall (filter pred c)))
      fname)))

(defn find-pattern2
  "Find instances of a a regular expression pattern in a file."
  [fname pattern]
  (let [pred (partial re-find pattern)
        reducer (fn [t v] (if (pred v) (conj t v) t))]
    (process-file 
      (fn [c] (reduce reducer [] c))
      fname)))

(defn write-file 
  "Sample text file writer"
  []
  (let [msg "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"]
    (with-open [w (writer "/data/bar.txt")]
      (dotimes [n 50000000] (.write w (str msg "\n"))))))

(->> "c:/data/vcld16gqamoap01-0331.csv"
  (process-file 
    #(doall 
       (reduce (fn [t v] (conj t [(split v #",")])) % [])))
  first
  (#(split % #",")))  
