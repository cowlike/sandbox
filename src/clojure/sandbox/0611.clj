(ns sandbox.0611)

(defn inter [xs ys]
  (concat xs ys))

(defn inter [xs ys]
  (loop [[x & xs] xs [y & ys] ys acc []]
    (cond
      (or (nil? x) (nil? y)) acc
      :else (recur xs ys (conj acc x y)))))

(def inter (partial mapcat list))
  
  
(= (inter [1 2 3] [:a :b :c]) '(1 :a 2 :b 3 :c))
