(ns sandbox.0528)

(defn inter [xs ys]
  (interleave xs ys))

(defn inter [xs ys]
  (if (and (not-empty xs) (not-empty ys))
    (concat [(first xs)] 
            [(first ys)] 
            (inter (rest xs) (rest ys)))))
  
(= (inter [1 2 3] [:a :b :c]) '(1 :a 2 :b 3 :c))


(defn fac [x]
  (if (> x 0)
    (* x (fac (dec x)))
    1))

(defn fac [x]
  (reduce * (take x (drop 1 (range)))))
  
(= (fac 8) 40320)

(defn dedup [xs]
  (if (not-empty xs)
    (if (seq? xs)
      (if (= (first xs) (second xs))
        (dedup (rest xs))
        (conj (dedup (rest xs)) (first xs)))
      xs)))
  
(defn dedup [xs]
  (reduce #(if (= (last %1) %2) %1 (conj %1 %2)) [] xs))

(defn dedup [xs]
  (map first (partition-by identity xs)))

  (defn f [t v]
    (if (= (last t) v) t (conj t v)))

  (= (dedup [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))