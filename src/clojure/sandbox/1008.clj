(ns sandbox.1008)

(defn add-to [xs x]
  (if (some #(= x %) xs) xs (conj xs x)))

(defn remdup [xs]
  (reduce (fn [t v] 
            (if (some #(= v %) t) 
              t 
              (conj t v))) 
          [] xs))


(= (remdup [1 2 1 3 1 2 4]) [1 2 3 4])

(defn mycomp [& fs]
  (fn [& args] 
    (first (reduce (fn [t v] [(apply v t)]) args (reverse fs)))))

(= [3 2 1] ((mycomp rest reverse) [1 2 3 4]))

(defn part [n xs]
  (loop [acc [] n n xs xs]
    (if (or (empty? xs) (< (count xs) n)) 
      acc
      (recur (conj acc (take n xs)) n (drop n xs)))))


(= (part 3 (range 8)) '((0 1 2) (3 4 5)))
(= (part 2 (range 8)) '((0 1) (2 3) (4 5) (6 7)))