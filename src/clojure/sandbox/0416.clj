(ns sandbox.0416)

(defn hi [#^String greet #^Integer name]
  (println (str greet ", " name)))

(def add-42 (partial + 42))

(->> "test" first int add-42)
(-> "test" first int add-42)
(add-42 (int (first "test")))

(def transform-first-letter (comp add-42 int first str))

(nth '(4 5 6 7) 2)
(#(apply + (for [v %] 1)) [1 2 3 4])
(#(apply + (for [v %] 1)) [1 2 3 4])
(#(reduce (fn [t v] (inc t)) 0 [1 2 3 4]))

(defn mycount [c] 
  (loop [c c count 0] 
    (if (empty? c) 
      count 
      (recur (rest c) (inc count))))) 
 
(defn mynth [c idx]
  (last (take (inc idx) c)))

(defn mynth2 [c idx]
  (first (drop idx c)))

(defn mynth3 [c idx]
  (loop [c c idx idx count 0]
    (if (= count idx)
      (first c)
      (recur (rest c) idx (inc count)))))

(defn mynth4 [c idx]
  (loop [c c idx idx]
    (if (zero? idx)
      (first c)
      (recur (rest c) (dec idx)))))


(defn mydrop [c n]
  (loop [c c n n]
    (if (= n 0)
      c
      (recur (rest c) (dec n)))))