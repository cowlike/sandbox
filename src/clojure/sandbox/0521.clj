(ns sandbox.0521)

;;4clojure 28

(defn myflatten [xs]
  (loop [xs xs acc []]
    (prn xs acc)
    (if (empty? xs)
      acc
      (cond
        (coll? (first xs)) (recur (concat (first xs) (rest xs)) acc)
        :otherwise (recur (rest xs) (conj acc (first xs)))))))

(defn myflatten [xs]
  (loop [xs xs acc []]
    (prn xs acc)
    (if (empty? xs)
      acc
      (cond
        (coll? (first xs)) (recur (first xs) acc)
        :otherwise (recur (rest xs) (conj acc (first xs)))))))

(defn myflatten [xs]
  (remove coll? (tree-seq coll? identity xs)))

(defn myflatten [xs]
  (if (coll? xs)
    (mapcat myflatten xs)
    [xs]))

(= (myflatten '((1 2) 3 [4 [5 6]])) '(1 2 3 4 5 6))

;; tail recursive
(defn add [xs]
  (loop [xs xs acc 0]
    (if (empty? xs) 
      acc 
      (recur (rest xs) (+ acc (first xs))))))

;; not tail recursive
(defn add [xs]
  (if (empty? xs) 
    0 
    (+ (first xs) (add (rest xs)))))
