(defn frazz [n]
  (letfn [(m3? [x] (if (zero? (mod x 3)) "fizz"))
          (m5? [x] (if (zero? (mod x 5)) "buzz"))]
    (str n " " (m3? n) (m5? n))))

(defn hof [n]
  #(+ % n))

(defn hof2 [n]
  (fn [x] (+ x n)))

(sort 
  (rest 
    (reverse [2 5 4 1 3 6])))

(-> [2 5 4 1 3 6]
  (reverse)
  (rest)
  (sort))

(defn foo [x]
  (let [x "sherry"]
    (prn (str "hi" x)))
  (prn x))


(defn add [c]
  (if (empty? c)
    0
    (+ (first c) (add (rest c)))))

(defn add-t [c total]
  (if (empty? c)
    total
    (recur (rest c) (+ total (first c))))) 
    
(defn add-t-loop [c]
  (loop [c c total 0]
    (if (empty? c)
      total
      (recur (rest c) (+ total (first c)))))) 
    

(defn foo [x]
  (loop [x x result []]
    (if (> x 0)
      (recur (dec x) (conj result (+ 2 x)))
      result)))

(defn mkmap [v ks]
  (into {} (for [k ks] {k v})))

(defn mkmap [v ks]
  (reduce #(assoc %1 v) {} ks))
