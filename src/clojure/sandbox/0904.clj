(ns sandbox.0904)

(defn m? [[x y z]]
  (letfn [(swap [[a b c]] 
            (conj [] 
                  a 
                  (if (vector? c) (swap c) c) 
                  (if (vector? b) (swap b) b)))] 
    (cond
      (and (vector? y) (vector? z)) (= y (swap z))
      (= y z) true 
      :else false)))

(fn mirror-tree?
  ([[_ left right]] 
    (mirror-tree? left right))
  ([t1 t2]
    (or 
      (and (nil? t1) (nil? t2))
      (let [[v1 l1 r1] t1
            [v2 l2 r2] t2]
        (and (= v1 v2) 
             (mirror-tree? l1 r2)
             (mirror-tree? r1 l2))))))