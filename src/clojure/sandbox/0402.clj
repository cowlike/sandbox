(ns sandbox.0402)

(= 2 (:foo {:bar 0, :baz 1} 2))

 #{1 \a "aaa"} ;set elements do not have to be the same type
 
; (defn addkv [v keys]
;   {})
 
(defn addkv [v keys]
  (reduce (fn [t el] (assoc t el v)) {} keys))
 
(= (addkv 0 [:a :b :c]) {:a 0 :b 0 :c 0})
 
(into (sorted-map) [ [:a 1] [:c 3] [:b 2] ])
(into {} [ [:a 1] [:c 3] [:b 2] ] )
 
(-> {:a 1}
  (assoc :b 2 :c 3)
  (dissoc :c))
 
(reduce + [1 2 3 4])
 
;;(fn [t v] (+ t v))
 
(reduce (fn [t v] (str t "<li>" v "</li>")) "" [1 2 3 4]) ; <li>1</li><li>2</li>...
 
(reduce (fn [t el] (assoc t el "foo")) {} "abcdef")

;;4clojure 20 alternatives
#(nth % (dec (dec (count %)))) 
#(->> % reverse (drop 1) first)
(comp first (partial drop 1) reverse)
#(let [[_ x] (reverse %)] x)

;; REPL history
;;
;> #{1 \a "aaa"}
;#{\a 1 "aaa"}
;=> #{1 \a "aaa" 1}
;IllegalArgumentException Duplicate key: 1  clojure.lang.PersistentHashSet.createWithCheck (PersistentHashSet.java:68)
;=> (= 2 (:foo {:bar 0, :baz 1} 2))
;true
;#<Namespace sandbox.0402>
;CompilerException java.lang.RuntimeException: Unable to resolve symbol: = in this context, compiling:(C:\Users\u273693.GLOBAL\AppData\Local\Temp\form-init324313497192187078.clj:2:1) 
;#<Namespace sandbox.0326>
;=> ({:a 1 :b 2} :a)
;1
;=> (:a {:a 1 :b 2})
;1
;=> (assoc {:a 1} :b 0)
;{:b 0, :a 1}
;=> (assoc {:a 1} :b 0 :c 3 :d \x)
;{:d \x, :c 3, :b 0, :a 1}
;#<Namespace sandbox.0402>
;CompilerException java.lang.RuntimeException: Unable to resolve symbol: defn in this context, compiling:(C:\Users\u273693.GLOBAL\AppData\Local\Temp\form-init324313497192187078.clj:2:1) 
;#<Namespace sandbox.0326>
;CompilerException java.lang.IllegalArgumentException: Duplicate key: 1, compiling:(sandbox\0402.clj:5:17) 
;CompilerException java.lang.RuntimeException: Unable to resolve symbol: __ in this context, compiling:(sandbox\0402.clj:7:5) 
;#<Namespace sandbox.0402>
;CompilerException java.lang.RuntimeException: Unable to resolve symbol: addkv in this context, compiling:(C:\Users\u273693.GLOBAL\AppData\Local\Temp\form-init324313497192187078.clj:2:4) 
;#<Namespace sandbox.0326>
;CompilerException java.lang.RuntimeException: Unable to resolve symbol: addkv in this context, compiling:(sandbox\0402.clj:7:5) 
;false
;#<Namespace sandbox.0402>
;false
;#<Namespace sandbox.0326>
;=> (addkv 0 [:a :b ])
;CompilerException java.lang.RuntimeException: Unable to resolve symbol: addkv in this context, compiling:(C:\Users\u273693.GLOBAL\AppData\Local\Temp\form-init324313497192187078.clj:1:1) 
;=> (in-ns 'sandbox.0402)
;#<Namespace sandbox.0402>
;=> (addkv 0 [:a :b ])
;{}
;{:a 1, :c 3, :b 2}
;=> (conj '() 1)
;(1)
;=> (conj (conj '() 1) 2)
;(2 1)
;=> (conj '(1 2 3) 1)
;(1 1 2 3)
;=> (conj [1 2 3] 1)
;[1 2 3 1]
;=> (first {:a 1 :b 2})
;[:b 2]
;=> (type (first {:a 1 :b 2}))
;clojure.lang.MapEntry
;=> (val (first {:a 1 :b 2}))
;2
;{:c 3, :b 2, :a 1}
;{:b 2, :a 1}
;{:b 2, :a 1}
;#'sandbox.0402/addkv
;#'sandbox.0402/addkv
;true
;10
;"1234"
;"<li>"
;"<li>4</li>"
;"<li>4</li>"
;"12</li>3</li>4</li>"
;"1<li>2</li><li>3</li><li>4</li>"
;"<li>1</li><li>2</li><li>3</li><li>4</li>"
;"1<li>2</li><li>3</li><li>4</li>"
;"<li>1</li><li>2</li><li>3</li><li>4</li>"
;{\f "foo", \e "foo", \d "foo", \c "foo", \b "foo", \a "foo"}
;{\f "foo", \e "foo", \d "foo", \c "foo", \b "foo", \a "foo"}
;=> (take 3 (range))
;(0 1 2)
;=> size
;CompilerException java.lang.RuntimeException: Unable to resolve symbol: size in this context, compiling:(C:\Users\u273693.GLOBAL\AppData\Local\Temp\form-init324313497192187078.clj:1:1043) 
;=> count
;#<core$count clojure.core$count@710421ed>
;=> (count [1 2 3])
;3
;=> (let [c [1 2 3]
;         l (count c)]
;     l)
;3
;=> (let [c [1 2 3]
;         l (count c)]
;     c)
;[1 2 3]
;=> (let [c [1 2 3]
;         l (count c)]
;     (nth c (dec l)))
;3
;=> (let [c [1 2 3]
;         l (count c)]
;     (dec l))
;2
;=> (let [c [1 2 3]
;         l (count c)]
;     (nth c (dec (dec l))))
;2
;=> (let [c (list 1 2 3 4 5)
;         l (count c)]
;     (nth c (dec (dec l))))
;4
;=> (let [c ["a" "b" "c"]
;         l (count c)]
;     (nth c (dec (dec l))))
;"b"
