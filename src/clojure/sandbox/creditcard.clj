(ns sandbox.creditcard)

;http://www.seas.upenn.edu/~cis194/spring13/hw/01-intro.pdf

(defn to-int [c]
  (- (int c) 48))

(defn to-digits [n]
  (if (<= n 0) '()
    (map to-int (str n))))

(defn do-double [xs]
  (if (= 2 (count xs)) (conj [] (first xs) (* 2 (last xs))) xs))

(defn double-every-other [xs]
  (reverse (map * (reverse xs) (cycle [1 2]))))

(defn sum-digits [xs]
  (apply + (mapcat to-digits xs)))

(defn valid? [n]
  (-> n 
    to-digits
    double-every-other
    sum-digits
    (mod 10)
    zero?))

    