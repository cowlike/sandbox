(ns sandbox.0821)

(defn card [[s r]]
  (let [suits {\D :diamond \H :heart \C :club \S :spade}
        rank (zipmap "23456789TJQKA" (range 13))]
    (hash-map :suit (suits s) :rank (rank r))))

(= {:suit :diamond :rank 10} (card "DQ"))

;; Unfinished homework
;;
;(defn lcm [& xs]
;  (let [multiples (fn [n] (map (partial * n) (rest (range))))
;        mults (map multiples xs)]
;    (loop [mults mults]
;      (if (apply = (map first mults))
;        (ffirst mults)
;        (recur (map drop-all-lowest mults))))))
    
;(defn drop-all-lowest [xs]
;  (let [least (apply min (map first xs))]
;    (map #(drop (if (= least (first %)) 1 0) %) xs)))
(defn lcm [& xs]
  (let [multiples (fn [n] (iterate (partial + n) n))
        mults (map multiples xs)]
    (loop [mults mults]
      (let [heads (map first mults)
        least (apply min heads)]
        (if (apply = heads)
          (ffirst mults)
          (recur (map #(drop (if (= least (first %)) 1 0) %) 
                      mults)))))))

(defn lcm [& xs]
  (loop [ys xs]
    (if (apply = ys)
      (first ys)
      (let [least (apply min ys)]
        (recur (map (fn [x y] (if (= y least) (+ x y) y))
                    xs ys))))))

(defn lcm [& n]
  (letfn [(gcd [a b] (cond 
                       (zero? b) a 
                       :else (gcd b (mod a b))))]
    (reduce (fn [a b] (/ (* a b) (gcd a b))) n)))

(== (lcm 2 3) 6)

(== (lcm 5 3 7) 105)


