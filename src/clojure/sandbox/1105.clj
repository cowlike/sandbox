(ns sandbox.1105)

(defn camelcase [s]
  (let [xs (clojure.string/split s #"-")]
    ( apply str (concat (first xs) (mapcat clojure.string/capitalize (rest xs))))))


(= (camelcase "multi-word-key") "multiWordKey")
(= (camelcase "something") "something")

(defn gcd2 [x y]
  (if (= y 0) x 
    (gcd y (mod x y))))

(defn tot [z]
  (letfn [(gcd [x y]
            (if (= y 0) x 
              (gcd y (mod x y))))]
    (count (filter #(= 1 %) (map #(gcd % z) (range z))))))

(= (tot 10) (count '(1 3 7 9)) 4)

(defn happynum [x]
  (if (= x 1) true
    (recur (reduce (fn [t v] (+ t v)) (loop [acc '() x x]
                                        (if (= x 0) acc
                                          (recur (let [y (mod x 10)] (conj acc (* y y))) (quot x 10))))))))

(= (happynum 986543210) true)