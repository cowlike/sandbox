(ns sandbox.0917)

(defn flip [f]
  (fn [a b] (f b a)))

(defn flip [f]
  (fn [& args] (apply f (reverse args))))

(= [1 2 3] ((flip take) [1 2 3 4 5] 3))

(defn fmap [m]
  (into {} (for [[x y] m [a b] y] [[x a] b])))

(= (fmap '{a {p 1, q 2}
         b {m 3, n 4}})
   '{[a p] 1, [a q] 2
     [b m] 3, [b n] 4})

(defn pdisj [s]
  (= (reduce + (map count s)) (count (reduce clojure.set/union s))))

(= (pdisj #{#{\U} #{\s} #{\e \R \E} #{\P \L} #{\.}})
   true)