(ns sandbox.0717)

(def foo 3)

(defn power [y]
  (fn [x] (int (Math/pow x y))))

(defn mycube []
  (fn [x] (int (Math/pow x foo))))

(= [1 8 27 64] (map (power 3) [1 2 3 4]))

(defn greet [greeting]
  (fn [name] (str greeting ", " name "!")))

(def ch-hi (greet "nihao"))
(def in-hi (greet "namaste"))

;#'sandbox.0717/ch-hi
;#'sandbox.0717/in-hi
;=> (ch-hi "Sherry")
;"nihao, Sherry!"
;=> (in-hi "Sherry")
;"namaste, Sherry!"
;=> ((greet "hola") "john")
;"hola, john!"


(defn prod-dig [x y]
  (map #(- (int %) (int \0)) 
       (seq (str (* x y)))))

(= (prod-dig 99 9) [8 9 1])

(defn cart [s1 s2]
  (into #{} (for [x s1 y s2] [x y])))

(= (cart #{1 2 3} #{4 5})
   #{[1 4] [2 4] [3 4] [1 5] [2 5] [3 5]})

;=> (reduce (fn [t v] (* 2 v)) 1 [1 2 3])
;6
;=> (reduce (fn [t v] (+ t (* 2 v))) 0 [1 2 3])
;12

(defn group [f xs]
  (reduce (fn [acc v] 
            (assoc acc 
                   (f v) 
                   (conj (vec (acc (f v))) v))) 
          {} xs))

(= (group #(> % 5) [1 3 6 8]) {false [1 3], true [6 8]})





