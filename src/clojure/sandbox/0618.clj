(ns sandbox.0618)

(defn dedup [xs]
  (reduce 
    (fn [acc v]
      (if (= (last acc) v) 
        acc
        (conj acc v))) 
    [] xs))
  
(defn dedup [xs]
  (let [acc []]
    (map
      #(conj acc %) xs)))

;;dedup
#(map first (partition-by identity %))

(= (dedup [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))


(defn rep [xs n]
  (mapcat #(repeat n %) xs))

(= (rep [1 2 3] 2) '(1 1 2 2 3 3))


(defn inter [v xs]
  (butlast (mapcat vector xs (repeat v))))

(defn inter [x s]
  (butlast (interleave s (repeat x))))

(= (inter 0 [1 2 3]) [1 0 2 0 3])

(defn dropnth [xs n]
  (mapcat #(take (dec n) %) (partition-all n xs)))
    
(defn dropnth [xs n]
  (comp (partial drop 1) (partial (take (dec n)))))

(= (dropnth [1 2 3 4 5 6 7 8] 3) [1 2 4 5 7 8])