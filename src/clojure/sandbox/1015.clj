(ns sandbox.1015)

(defn jxt [& fs]
  (fn [& xs] 
    (reduce 
      (fn [t v] (conj t (apply v xs))) 
      [] 
      fs)))

(= [21 6 1] ((jxt + max min) 2 3 5 1 6 4))

(defn wsort [s]
  (sort-by clojure.string/lower-case            
           (-> (clojure.string/replace s #"[^a-zA-Z ]*" "")
             (clojure.string/split #" "))))

(defn wsort [s]
  (as-> s arg
    (clojure.string/replace arg #"[^a-zA-Z ]*" "")
    (clojure.string/split arg #" ")
    (sort-by clojure.string/lower-case arg)))

(= (wsort  "Have a nice day.")
   ["a" "day" "Have" "nice"])

(defn primes [n]
  (letfn [(prime? [x]
            (not-any? zero? (map #(mod x %) (range 2 x))))]
    (take n (for [n (drop 2 (range)) :when (prime? n)]  n))))
    