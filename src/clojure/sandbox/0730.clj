(ns sandbox.0730)

(defn sdiff [s1 s2]
  (clojure.set/difference
    (clojure.set/union s1 s2)
    (clojure.set/intersection s1 s2)))

(let [u (clojure.set/union #{1 2 3 4 5 6} #{1 3 5 7})
         i (clojure.set/intersection #{1 2 3 4 5 6} #{1 3 5 7})]
     (clojure.set/difference u i))

(defn bin2num [s]  
  (reduce 
    (fn [acc v] (+ (- (int v) (int \0)) (* acc 2))) 
    0 s))

(defn bin2num [s]  
  (reduce 
    (fn [acc v] (+ (read-string v) (* acc 2))) 
    0 (map str s)))

(= 11 (bin2num "1011"))

(defn dot-prod [xs ys]
  (apply + (map * xs ys)))

(= 32 (dot-prod [1 2 3] [4 5 6]))

(defn calc [& args]
  (:x (reduce (fn [t v]
                (if (fn? v) 
                  (assoc t :func v)
                  (assoc t :x ((:func t) (:x t) v))))
              {:x (first args)}
              (rest args))))
  
(defn me
  ([x f y] (f x y))
  ([x f y & r] (apply me (f x y) r)))

(= 8  (me 10 / 2 - 1 * 2))