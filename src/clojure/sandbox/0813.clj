(ns sandbox.0813)

(defn tree? [xs] 
  (if (and (coll? xs) (= 3 (count xs)))
    (let [[_ left right] xs]
      (cond 
        (not (nil? left)) (tree? left)
        (not (nil? right)) (tree? right)
        :else true))
    false))
    

(= (tree? '(:a (:b nil nil) nil))
   true)

(= (tree? [1 [2 [3 [4 false nil] nil] nil] nil])
   false)

(defn sq [n]
  (apply + (map (fn [x] (let [y (- (int x) (int \0))] (* y y)))
                  (str n))))

(defn sum-sq-digit [xs]
  (letfn [(sq [n]
            (apply + (map (fn [x] (let [y (read-string (str x))] (* y y)))
                          (str n))))]
    (reduce (fn [t v] (if (> (sq v) v) (inc t) t)) 0 xs)))

(= 19 (sum-sq-digit (range 30)))