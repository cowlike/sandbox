(ns sandbox.0514)

;;prob 38
(defn mymax [& args]
  (reduce 
    (fn [acc v] 
      (if (> v acc) v acc)) 
    args))

(= (mymax 45 67 11) 67)

(= (mymax 30 20) 30)

;;prob 29
(defn caps [s]
  (apply str 
         (filter #(and 
                    (>= (int %) 65) 
                    (<= (int %) 90)) 
                 s)))
;;prob 29 alternate
#(apply str (re-seq #"[A-Z]" %))

(= (caps "HeLlO, WoRlD!") "HLOWRD")

;;prob32
;;nope!
(defn dup [s]
  (concat s s))

;;not quite
(defn dup [s]
  (flatten (map #(vector % %) s)))

;;works!
(defn dup [s]
  (reduce (fn [acc v] (conj acc v v)) [] s))

;;also
#(interleave % %)
mapcat (fn [o] (list o o))

(dup [[1 2] [3 4]])
(= (dup [1 2 3]) '(1 1 2 2 3 3))

;;prob 34
(defn myrange [start end]
  (loop [start start acc []]
    (if (= start end) 
      acc
      (recur (inc start) (conj acc start)))))

(= (myrange 1 4) '(1 2 3))

