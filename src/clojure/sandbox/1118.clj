(ns sandbox.1118)

;;test comment

(defn balnth? [n]
  (let [xs (map #(- (int %) 48) (str n))
        len (quot (count xs) 2)
        f #(apply + (take len %))]
    (= (f xs) (f (reverse xs)))))


(defn mytrmpln 
  ([f & xs]      
    (let [res (apply f xs)]
      (if (fn? res) (mytrmpln (res)) res)))  
  ([f]
    (if (fn? f) (recur (f)) f)))


(defn mytrmpln2 [f & args]
  (loop [f (apply f args)]
    (if (fn? f) (recur (f)) f)))

(= (letfn [(triple [x] #(sub-two (* 3 x)))
          (sub-two [x] #(stop?(- x 2)))
          (stop? [x] (if (> x 50) x #(triple x)))]
    (mytrmpln2 triple 2))
  82)

; test
;test branch changes



