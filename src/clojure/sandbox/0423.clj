(ns sandbox.0423)

(defn mycount [xs]
  (reduce (fn [t v] (inc t)) 0 xs))

(= (mycount "Hello World") 11)

(fn [xs]
  (apply + (map (fn [_] 1) xs)))

(defn f [xs]
  (reduce str (for [_ xs] 1)))

(= (filter odd? #{1 2 3 4 5}) '(1 3 5))

;;NO!!!!!
(defn myreverse [xs]
  (let [acc [\a]]
    (for [x xs] (conj acc x))))

;(defn myreverse [xs]
  
(defn myreverse [xs]
  (loop [xs xs acc []]
    (if (empty? xs)
      acc
      (recur (rest xs) (cons (first xs) acc)))))
  
(= (myreverse [1 2 3 4 5]) [5 4 3 2 1])