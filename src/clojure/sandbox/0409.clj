(ns sandbox.0409)

;=> (= [nil 1 2 nil] [1 2])
;false
;=> (count [nil 1 2 nil])
;4
;=> (map identity [nil 1 2 nil])
;(nil 1 2 nil)
;=> (filter (complement nil?) [nil 1 2 nil])
;(1 2)
;=> (filter (comp not nil?) [nil 1 2 nil])
;(1 2)

(int (first "test"))
(->> "test"
     first
     int)
(-> "test"
     first
     int)

(defn foo [a b c]
  (println "a = " a ", b = " b ", c = " c))

(-> 1
     (foo 2 3))

(->> 1
     (foo 2 3))

(defn bar [x]
  (foo x 2 x))

(->> 1
     (#(foo % 2 %)))

(->
     \x
     (#(foo 2 % 3)))

(:foo #{:a 1 :b 2} "foo!")


(defn mkmap [value ks]
  (reduce (fn [acc v] (assoc acc v value)) {} ks))

(defn mkmap2 [value ks]
  (into {} (for [k ks] {k value})))

(= {:a 0 :b 0 :c 0} (mkmap2 0 [:a :b :c]))

(for [x [1 2 3 4] :when (odd? x)] (* x x))

(for [x [1 2 3] y [5 6] z [7 8]] [x y z])

;=> (= (#(let [[_ x] (reverse %)] x) (list 1 2 3 4 5)) 4)
;true
;=> (let [[a b & more] [1 2 3 4]]
;     b)
;2
;=> (let [[a b & more] [1 2 3 4]]
;     [b more])
;[2 (3 4)]

