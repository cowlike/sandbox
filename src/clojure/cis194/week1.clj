(ns cis194.week1)

;http://www.seas.upenn.edu/~cis194/spring13/hw/01-intro.pdf

;Exercise 1 
;We need to first find the digits of a number

(defn to-int [c]
  (- (int c) 48))

(defn to-digits [n]
  (if (<= n 0) '()
    (map to-int (str n))))

;Exercise 2
;Once we have the digits in the proper order, we need to
;double every other one
(defn do-double [xs]
  (if (= 2 (count xs)) (conj [] (first xs) (* 2 (last xs))) xs))

(defn double-every-other [xs]
  (reverse (map * (reverse xs) (cycle [1 2]))))

;Exercise 3
;The output of doubleEveryOther
;has a mix of one-digit
;and two-digit numbers. Define the function
;sumDigits :: [Integer] -> Integer
;to calculate the sum of all digits

(defn sum-digits [xs]
  (apply + (mapcat to-digits xs)))

;Exercise 4
;Define the function
;validate :: Integer -> Bool
;that indicates whether an Integer
;could be a valid credit card number.
;This will use all functions defined in the previous exercises.
;Example:
;validate 4012888888881881 = True
;Example:
;validate 4012888888881882 = False

(defn valid? [n]
  (-> n 
    to-digits
    double-every-other
    sum-digits
    (mod 10)
    zero?))

;; Exercise 5 The Towers of Hanoi
;is a classic puzzle with a solution
;that can be described recursively. Disks of different sizes are stacked
;on three pegs; the goal is to get from a starting configuration with
;all disks stacked on the first peg to an ending configuration with all
;disks stacked on the last peg

(defn hanoi [num a b c]
  (when (pos? num)
    (concat
      (hanoi (dec num) a c b)
      [[a b]]
      (hanoi (dec num) c b a))))