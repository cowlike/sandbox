(ns cis194.week2)

;; exercises from this Haskell class
;; http://www.seas.upenn.edu/~cis194/hw/02-lists.pdf

(def colors [:red :green :blue :yellow :orange :purple])


;; exact-matches :: [color] -> [color] -> int
(defn exact-matches [xs ys]
  (count 
    (filter true? (map #(= %1 %2) xs ys))))

(= 0 (exact-matches [:red :blue :green :yellow] [:blue :green :yellow :red]))
(= 2 (exact-matches [:red :blue :green :yellow] [:red :purple :green :orange]))

;; count-colors :: [color] -> [int]
(defn count-colors [xs]
  (let [m (group-by identity xs)]
    (map (fn [c] (if (contains? m c)
                   (count (m c))
                   0)) 
         colors)))

(defn cc [xs]
  (let [m (zipmap colors (repeat 0))]
    (vals (reduce (fn [t v] (assoc t v (inc (t v)))) m xs))))
  
(= (count-colors [:red :blue :yellow :purple]) [1, 0, 1, 1, 0, 1])
(= (count-colors [:green :blue :green :orange]) [0, 2, 1, 0, 1, 0])

;; matches :: [color] -> [color] -> int
(defn matches [xs ys]
  (count (clojure.set/intersection (set xs) (set ys))))

(defn matches' [xs ys]
  (let [cxs (count-colors xs) cys (count-colors ys)]
    (prn cxs cys)
    (count (filter true? 
                   (map (fn [x y] 
                          (and (not (zero? x))
                               (not (zero? y))))
                        cxs cys)))))

(= 3 (matches' [:red :blue :yellow :orange] [:blue :red :orange :orange]))

