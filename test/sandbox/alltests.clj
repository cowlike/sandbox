(ns sandbox.alltests
  (:require [clojure.test :refer :all]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [sandbox.4clojure :refer :all]
            [cis194.week1 :refer :all]))

(deftest fib-test
  (testing "4clojure problem 26"
           (is (= [1 1 2 3 5 8 13 21] (fib 8)))))

(deftest fib2-test
  (testing "4clojure problem 26 solutions are same"
           (is (= (fib 10) (fib2 10)))))

(def sort-idempotent-prop
  (prop/for-all [v (gen/vector gen/int)]
    (= (sort v) (sort (sort v)))))